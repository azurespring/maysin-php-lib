<?php

namespace AzureSpring\Maysin;

class User implements ReceiverInterface
{
    private $id;


    public function __construct( $id )
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}
