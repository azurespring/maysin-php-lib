<?php

namespace AzureSpring\Maysin;

use GuzzleHttp\Client as Guzzle;

class Client
{
    private $client;

    private $appkey;

    private $secret;


    /**
     * @param GuzzleHttp $client
     * @param string $appkey
     * @param string $secret
     */
    public function __construct( Guzzle $guzzle, $appkey, $secret )
    {
        $this->guzzle = $guzzle;
        $this->appkey = $appkey;
        $this->secret = $secret;
    }

    /**
     * send message
     *
     * @param Message $message
     * @param ReceiverInterface[] $receivers
     *
     * @return boolean
     */
    public function send( Message $message, array $receivers )
    {
        switch ( $message->getType() ) {
            case Message::HYPER:
                $response = $this->guzzle->request('POST', '/v1/official/send', [
                    'multipart' => $this->encode([
                        'key'   => $this->appkey,
                        'sign'  => $this->sign( $message->getCreatedAt()->getTimestamp() ),
                        'time'  => (string) $message->getCreatedAt()->getTimestamp(),
                        'body'  => json_encode([
                            'url'       => $message->getURL(),
                            'origin'    => $message->getURL(),
                            'image'     => $message->getImage(),
                            'title'     => $message->getHeadline(),
                            'brief'     => $message->getText(),
                        ]),
                        'meta'  => json_encode([ 'type' => $message->getType() ]),
                        'fans_id' => implode(',', array_map(
                            function (ReceiverInterface $recv) {
                                return $recv->getId();
                            },
                            $receivers
                        ))
                    ])
                ]);
                break;

            default:
                $response = $this->guzzle->request('POST', '/v1/official/send', [
                    'multipart' => $this->encode([
                        'key'   => $this->appkey,
                        'sign'  => $this->sign( $message->getCreatedAt()->getTimestamp() ),
                        'time'  => (string) $message->getCreatedAt()->getTimestamp(),
                        'text'  => $message->getText(),
                        'picture' => @fopen( $message->getImage(), 'r' ),
                        'meta'  => json_encode([ 'type' => $message->getType() ]),
                        'fans_id' => implode(',', array_map(
                            function (ReceiverInterface $recv) {
                                return $recv->getId();
                            },
                            $receivers
                        ))
                    ])
                ]);
                break;
        }

        return ( 200 == $response->getStatusCode() &&
                 ( $body = json_decode($response->getBody()) ) &&
                 0 == $body->error );
    }

    public function receive( $message, $sign, $nonce, $appkey = null )
    {
        if ( (!$appkey || $appkey == $this->appkey) &&
             $sign == $this->sign($nonce) )
            return Message::scrutinize( $message );
    }

    private function encode( array $form )
    {
        return array_map(
            function ($key) use ($form) {
                return [
                    'name'      => $key,
                    'contents'  => $form[$key]
                ];
            },
            array_keys(array_filter($form))
        );
    }

    private function sign( $nonce )
    {
        return sha1(sprintf( '%s-%s-%s', $this->appkey, $this->secret, $nonce ));
    }
}
