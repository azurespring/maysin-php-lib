<?php

namespace AzureSpring\Maysin;

class Message
{
    const PLAIN = 1;            // plain text
    const IMAGE = 2;            // picture
    const SOUND = 3;            // sound clip
    const MULTI = 4;            // multimedia
    const HYPER = 7;            // hyperlink

    private $id;

    private $from;

    private $text;

    private $image;

    private $url;

    private $headline;

    private $createdAt;


    public static function scrutinize( $data )
    {
        if ( is_string($data) )
            $data = json_decode( $data, true );
        $data = array_replace_recursive(
            array(
                'id'        => null,
                'sender_id' => null,
                'body'      => array(
                    'text'      => null,
                    'picture'   => array( 'source' => null ),
                    "audio"     => null,
                ),
                'created_at' => null,
            ),
            $data
        );

        return ((new Message())
                ->setId( $data['id'] )
                ->setFrom( new User($data['sender_id']) )
                ->setText( $data['body']['text'] )
                ->setImage( $data['body']['picture']['source'] )
                ->setCreatedAt( new \DateTime($data['created_at']) ));
    }

    public function setId( $id )
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setFrom( User $user )
    {
        $this->from = $user;

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setText( $text )
    {
        $this->text = $text;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setImage( $image )
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setURL( $url )
    {
        $this->url = $url;

        return $this;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function setHeadline( $headline )
    {
        $this->headline = $headline;

        return $this;
    }

    public function getHeadline()
    {
        return $this->headline;
    }

    public function setCreatedAt( \DateTime $createdAt )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getType()
    {
        if ( $this->url )
            return self::HYPER;

        if ( $this->image )
            return $this->text ? self::MULTI : self::IMAGE;

        return self::PLAIN;
    }
}
