<?php

namespace AzureSpring\Maysin\Tests;

use AzureSpring\Maysin\Message;

class MessageTest extends \Phpunit_Framework_Testcase
{
    public function testScrutinizeOk()
    {
        $message = Message::scrutinize( <<<'__JSON__'
{"body": {"text": "hello", "picture": {"large": "http://example.com/blobs/_640/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "source": "http://example.com/blobs/source/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "thumb": "http://example.com/blobs/320_534/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg"}}, "created_at": "2016-03-06 15:16:37.497390", "meta": {"type": 4, "alert": false}, "sender_id": 7, "id": 130420}
__JSON__
        );

        $this->assertEquals( 130420, $message->getId() );
        $this->assertEquals( 7, $message->getFrom()->getId() );
        $this->assertEquals( 'hello', $message->getText() );
        $this->assertEquals( 'http://example.com/blobs/source/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg', $message->getImage() );
        $this->assertEquals( new \DateTime('2016-03-06 15:16:37'), $message->getCreatedAt() );
    }
}
