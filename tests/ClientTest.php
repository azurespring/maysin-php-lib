<?php

namespace AzureSpring\Maysin\Tests;

use AzureSpring\Maysin\Client as Maysin;
use AzureSpring\Maysin\Message;
use AzureSpring\Maysin\User;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    private $maysin;

    private $guzzle;

    private $response;


    public function setUp()
    {
        $this->guzzle =
            $this
            ->getMockBuilder( 'GuzzleHttp\\Client' )
            ->getMock()
        ;
        $this->response =
            $this
            ->getMockBuilder( 'GuzzleHttp\\Psr7\\Response' )
            ->getMock()
        ;
        $this
            ->guzzle
            ->method( 'request' )
            ->willReturn( $this->response )
        ;

        $this->maysin = new Maysin( $this->guzzle,
                                    'iP5GPxOIhMkFmHDg',
                                    'cji7uXvG1XUHTudb' );
    }

    public function testSendTextOk()
    {
        $this
            ->guzzle
            ->expects( $this->once() )
            ->method( 'request' )
            ->with(
                $this->equalTo('POST'),
                $this->equalTo('/v1/official/send'),
                $this->equalTo([
                    'multipart' => [
                        [ 'name'        => 'key',
                          'contents'    => 'iP5GPxOIhMkFmHDg' ],
                        [ 'name'        => 'sign',
                          'contents'    => 'cb04d7a2aba1e17bbb401de1c31af2a133b410e5' ],
                        [ 'name'        => 'time',
                          'contents'    => '1451466475' ],
                        [ 'name'        => 'text',
                          'contents'    => 'hello, world.' ],
                        [ 'name'        => 'meta',
                          'contents'    => '{"type":1}' ],
                        [ 'name'        => 'fans_id',
                          'contents'    => '113' ]
                    ]
                ])
            )
        ;

        $this
            ->response
            ->method( 'getStatusCode' )
            ->willReturn( 200 )
        ;
        $this
            ->response
            ->method( 'getBody' )
            ->willReturn( '{"error":0}' )
        ;

        $ok = $this->maysin->send(
            (new Message())
            ->setText( 'hello, world.' )
            ->setCreatedAt( new \DateTime('2015-12-30T17:07:55+08:00') ),
            [ new User(113) ]
        );
        $this->assertTrue( $ok );
    }

    public function testSendImageOk()
    {
        $this
            ->guzzle
            ->expects( $this->once() )
            ->method( 'request' )
            ->with(
                $this->equalTo('POST'),
                $this->equalTo('/v1/official/send'),
                $this->callback(function (array $options) {
                    return
                        isset($options['multipart']) &&
                        (
                            $image = current(array_filter(
                                $options['multipart'],
                                function (array $param) {
                                    return
                                        isset($param['name']) &&
                                        isset($param['contents']) &&
                                        'picture' == $param['name']
                                    ;
                                }
                            ))
                        ) &&
                        is_resource( $image['contents'] ) &&
                        fstat( fopen(__FILE__, 'r') ) == fstat( $image['contents'] ) &&
                        [ 'multipart' => [[ 'name'        => 'key',
                                            'contents'    => 'iP5GPxOIhMkFmHDg' ],
                                          [ 'name'        => 'sign',
                                            'contents'    => 'cb04d7a2aba1e17bbb401de1c31af2a133b410e5' ],
                                          [ 'name'        => 'time',
                                            'contents'    => '1451466475' ],
                                          [ 'name'        => 'text',
                                            'contents'    => 'hello, world.' ],
                                          [ 'name'        => 'picture',
                                            'contents'    => $image['contents'] ],
                                          [ 'name'        => 'meta',
                                            'contents'    => '{"type":4}' ],
                                          [ 'name'        => 'fans_id',
                                            'contents'    => '113' ]] ] == $options
                    ;
                })
            )
        ;

        $this
            ->response
            ->method( 'getStatusCode' )
            ->willReturn( 200 )
        ;
        $this
            ->response
            ->method( 'getBody' )
            ->willReturn( '{"error":0}' )
        ;

        $ok = $this->maysin->send(
            (new Message())
            ->setText( 'hello, world.' )
            ->setImage( __FILE__ )
            ->setCreatedAt( new \DateTime('2015-12-30T17:07:55+08:00') ),
            [ new User(113) ]
        );
        $this->assertTrue( $ok );
    }

    public function testSendStatusError()
    {
        $this
            ->response
            ->method( 'getStatusCode' )
            ->willReturn( 500 )
        ;
        $this
            ->response
            ->expects( $this->never() )
            ->method( 'getBody' )
        ;

        $ok = $this->maysin->send(
            (new Message())
            ->setText( 'hello, world.' )
            ->setCreatedAt( new \DateTime('2015-12-30T17:07:55+08:00') ),
            [ new User(113) ]
        );
        $this->assertFalse( $ok );
    }

    public function testSendError()
    {
        $this
            ->response
            ->method( 'getStatusCode' )
            ->willReturn( 200 )
        ;
        $this
            ->response
            ->method( 'getBody' )
            ->willReturn( '{"error":-1}' )
        ;

        $ok = $this->maysin->send(
            (new Message())
            ->setText( 'hello, world.' )
            ->setCreatedAt( new \DateTime('2015-12-30T17:07:55+08:00') ),
            [ new User(113) ]
        );
        $this->assertFalse( $ok );
    }

    public function testSendHyperlinkOk()
    {
        $this
            ->guzzle
            ->expects( $this->once() )
            ->method( 'request' )
            ->with(
                $this->equalTo('POST'),
                $this->equalTo('/v1/official/send'),
                $this->equalTo([
                    'multipart' => [
                        [ 'name'        => 'key',
                          'contents'    => 'iP5GPxOIhMkFmHDg' ],
                        [ 'name'        => 'sign',
                          'contents'    => 'cb04d7a2aba1e17bbb401de1c31af2a133b410e5' ],
                        [ 'name'        => 'time',
                          'contents'    => '1451466475' ],
                        [ 'name'        => 'body',
                          'contents'    => '{"url":"http:\/\/example.com\/","origin":"http:\/\/example.com\/","image":"http:\/\/example.com\/sample.jpg","title":"headline","brief":"synopsis"}' ],
                        [ 'name'        => 'meta',
                          'contents'    => '{"type":7}' ],
                        [ 'name'        => 'fans_id',
                          'contents'    => '113' ]
                    ]
                ])
            )
        ;

        $this
            ->response
            ->method( 'getStatusCode' )
            ->willReturn( 200 )
        ;
        $this
            ->response
            ->method( 'getBody' )
            ->willReturn( '{"error":0}' )
        ;

        $ok = $this->maysin->send(
            (new Message())
            ->setURL( 'http://example.com/' )
            ->setImage( 'http://example.com/sample.jpg' )
            ->setText( 'synopsis' )
            ->setHeadline( 'headline' )
            ->setCreatedAt( new \DateTime('2015-12-30T17:07:55+08:00') ),
            [ new User(113) ]
        );
        $this->assertTrue( $ok );
    }

    public function testReceiveOk()
    {
        $message = $this->maysin->receive(
            <<<'__JSON__'
{"body": {"text": "hello", "picture": {"large": "http://example.com/blobs/_640/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "source": "http://example.com/blobs/source/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "thumb": "http://example.com/blobs/320_534/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg"}}, "created_at": "2016-03-06 15:16:37.497390", "meta": {"type": 4, "alert": false}, "sender_id": 7, "id": 130420}
__JSON__
            ,
            'e527d39f7717f5d1803a5244ecfed06c13f7fd6e',
            '1457248597',
            'iP5GPxOIhMkFmHDg'
        );

        $this->assertNotNull( $message );
    }

    public function testReceiveError()
    {
        $message = $this->maysin->receive(
            <<<'__JSON__'
{"body": {"text": "hello", "picture": {"large": "http://example.com/blobs/_640/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "source": "http://example.com/blobs/source/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg", "thumb": "http://example.com/blobs/320_534/39/fa/39fa99c4dc596533bda00a1ba4eee92a336a899f.jpg"}}, "created_at": "2016-03-06 15:16:37.497390", "meta": {"type": 4, "alert": false}, "sender_id": 7, "id": 130420}
__JSON__
            ,
            'e527d39f7717f5d1803a5244ecfed06c13f7fd6e',
            '1457248593',
            'iP5GPxOIhMkFmHDg'
        );

        $this->assertNull( $message );
    }
}
